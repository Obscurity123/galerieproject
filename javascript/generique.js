export function reduireArray(array, size) {
  if (array.length <= size) {
    return [array];
  }
  return [array.slice(0, size), ...reduireArray(array.slice(size), size)];
}

export const dateTimeFormat = Intl.DateTimeFormat("fr");