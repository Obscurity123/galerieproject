importScripts(
  "https://cdnjs.cloudflare.com/ajax/libs/localforage/1.7.3/localforage.min.js"
);

const cacheName = 'galerie';

const files = [
	'/',
	'./script.js',
	'./index.html',
	'./javascipt/afficher.js',
	'./javascipt/genereique.js',
	'./javascipt/like.js',
	'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
	'https://cdnjs.cloudflare.com/ajax/libs/localforage/1.7.3/localforage.min.js',
	"https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/pia22486-main.jpg",
	"https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/pia23952.jpg",
	"https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/pia21044_orig.jpg",
	"https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/pia23378-16.jpg",
	"https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/pia23346-16.jpg",
	"https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/pia23047_0.jpg"
];

self.addEventListener('install', (e) => {
	caches.open(cacheName).then((cache) => {
		cache.addAll(files);
	});
});
self.addEventListener('activate', (e) => {
	e.waitUntil(
		caches.keys().then(function (keyList) {
			return Promise.all(
				keyList.map(function (key) {
					if (key !== cacheName) {
						return caches.delete(key);
					}
				})
			);
		})
	);
});

self.addEventListener('fetch', (event) => {
	console.log(event.request.url);
	const url = event.request.url;

	if (navigator.onLine && url.indexOf("http://localhost:3000") != 0) {
		fetch(event.request).then((n) => {
			caches.open(cacheName).then((s) => s.put(event.request, n));
		});
	}

	if (url.indexOf('https://zen-curie-baff5a.netlify.app/images.json') === 0) {
		event.respondWith(
			fetch(event.request).then((response) => {
				if (response.status === 200) {
					console.info('Formatting data');
					return response.json().then((json) => {
						const formattedResponse = json.map((j) => ({
							name: j.name,
							url: j.url,
							description: j.description || '',
							updated_at: j.updated_at,
							id: j.id
						}));

						return new Response(JSON.stringify(formattedResponse));
					});
				} else {
					console.error('Service Worker', 'Error when fetching', event.request.url);
					return response;
				}
			})
		);
	} else {
		event.respondWith(
			caches
			.open(cacheName)
			.then((cache) => {
				cache.match(event.request);
			})
			.then((response) => response || fetch(event.request))
		);
	}
});

self.addEventListener('sync', (event) => {
	if (event.tag === 'syncLikes') {
		console.log('Synchronisation en cours');

		if (Notification.permission === 'granted') {
			registration.showNotification('Synchronisation en cours');
		}

		event.waitUntil(
			localforage.getItem('likes').then((likes) => {
				return fetch('http://localhost:3000/likes', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify(likes)
				});
			})
		);
	}
});