## Site en ligne sur netlify

[https://zen-curie-baff5a.netlify.app/](https://zen-curie-baff5a.netlify.app/)

## Pour l'installation

```
npm i
cd server
npm i
```

## Pour lancer le serveur

```
cd server
node index.js
```

## Pour lancer l'application en local

```
npx webpack
npx webpack serve
```
